<?php
session_start();
include_once "library/users.php";
include_once "library/reservation.php";

/* Contains miscelaneous class implementation, event validation */

$opt = $_REQUEST["opt"];

switch ($opt) {
	case "login":
		$email = $_POST["email"];
		$passwd = $_POST["passwd"];
		$clsUsers = new Users();
		if ($clsUsers->isLoginSuccess($email, $passwd)) {
			echo "1";
		} else {
			echo "0";
		}
		break;
	case "validateReservation":
		$checkin_date = $_POST["checkin"];
		$hour_in = $_POST["hour_in"];
		$hour_out = $_POST["hour_out"];
		
		if ($checkin_date == date("Y-m-d")) {
			if ($hour_in > date("h")) {
				echo "1";
				exit();
			}
		} elseif ($checkin_date < date("Y-m-d")) {
			echo "1";
			exit();
		}
		$objReservation = new Reservation();
		$isExist = $objReservation->isExist($checkin_date, $hour_in, $hour_out);
		if ($isExist) {
			echo "0";
		} else {
			echo "1";
		}
		break;
}

?>