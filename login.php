<?php
session_start();
include_once "library/users.php";

/* Login Page contains Email & Password input.
On Form submition, it checks for Email/Password match and after that the Session data
In case of there was reserve event previously then go to reservation save page otherwise index page */

if (isset($_POST["fill"])) {
	$email = $_POST["email"];
	$objUsers = new Users();
	$objUser = $objUsers->userDetail($email);
	$_SESSION["UserID"] = $objUser->get_ID();
	$_SESSION["name"] =$objUser->get_Name();
	
	if ($_SESSION["checkin_date"] <> "") {
		Header("Location: reservation_save.php");
	} else {
		Header("Location: index.php");
	}
	exit();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				
			});
			
			function Validate() {
				var OK = false;
				$.ajax( {
					async: false,
					url: "static_funcs.php?opt=login",
					type: "post",
					data: $("#frmlogin").serialize(),
					success: function(response) {	
						//alert(response);
						if ($.trim(response)=="1") {
							OK = true;
							return OK;
						}
						
					}		
					
				});
				return OK;
			}
		</script>
    </head>
    <body>
    
        <div id="boxLogin" class="boxContent" style="display: block;">
            <form id="frmlogin" onSubmit="return Validate()" method="post" action="login.php">
				<input type='hidden' name='fill' value='1'>
                <table border="0">
                    <tr>
                        <td><h4>Sign In To Reserve</h4><hr></td>
                    </tr>
                    
                    <tr>
                        <td><b>Email</b></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="text" id="email" name="email"></td>
                    </tr>
                    <tr>
                        <td><b>Password</b></td>
                    </tr>
                    <tr>
                        <td><input type="password" class="text" id="passwd" name="passwd"></td>
                    </tr>
                    <tr>
                        <td><input type="submit" class="button" id="btnlogin" value="Sign In"></td>
                    </tr>
                    <tr>
                        <td align='right'><a href='addUser.php'>Free Sign Up</a></td>
                    </tr>
                </table>
              </form>
         </div>
    </body>
</html>
