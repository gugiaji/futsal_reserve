<?php
session_start();
include_once "library/reservation.php";
$objReservation = new Reservation();
$objReservation->add($_SESSION["UserID"], $_SESSION["checkin_date"], $_SESSION["hour_in"], $_SESSION["hour_out"]);

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css" />
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				
			});
			
			
		</script>
    </head>
    <body>
    <center>
        <div id="header">
			<table style="width:100%" border=0 cellspacing=0 cellpadding=0><tr><td>
				<h2>Welcome To Futsal Court Reservation</h2>
			</td>
				<td align="right">
				<?php				
					echo $_SESSION["name"];
				?></td>
			</tr>
			</table>
		</div>
    </center>
        <div id="content" class="boxContent" style="display: block;">
			Your Time has been successfully booked.<br>
			<a href='index.php'>Home</a>			
         </div>
		 <?php
		 
			$_SESSION["checkin_date"] = "";
			$_SESSION["hour_in"] = "";
			$_SESSION["hour_out"] = "";
			?>
    </body>
</html>