<?php
/* Handles Query Read & Write. 
Encapsulate Connection setting in Class implementation */

include_once 'settings.php';
class database {
    private $conn;
    function __construct() {
        $this->conn = mysql_connect($GLOBALS["ip_addr"],$GLOBALS["user"],$GLOBALS["pwd"]);
        mysql_select_db($GLOBALS["db_name"]);
    }
    
    function read($sql) {
        $result = mysql_query($sql);        
        $fncResult = array();
        while ($row= mysql_fetch_array($result)) {
            $rowResult = array();
            $i=0;
            while ($i < mysql_num_fields($result)) {
                $meta = mysql_fetch_field($result,$i);
                $rowResult[$meta->name] = $row[$i];
				
                $i++;
            }
            $fncResult[] = $rowResult;
			
        }
        mysql_free_result($result);
        mysql_close($this->conn);
		return $fncResult;
    }
    
    function write($sql) {
        mysql_query($sql);   
        mysql_close($this->conn);
    }
}
?>

