<?php
session_start(); 
/* Landing Page contains reserve futsal court form
On form submit, it checks Session data
In case no login then go to Login page otherwise to reservation save page */

if (isset($_POST["fill"])) {
	$_SESSION["checkin_date"] = $_POST["checkin"];
	$_SESSION["hour_in"] = $_POST["hour_in"];
	$_SESSION["hour_out"] = $_POST["hour_out"];
	if ($_SESSION["UserID"] == "") {
		Header("location: login.php");
		
	} else {
		Header("location: reservation_save.php");
	}
	exit();
}

?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
		<link rel="stylesheet" type="text/css" href="css/jquery-ui-1.8.13.custom.css" />
        <script type="text/javascript" src="js/jquery-1.6.2.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.14.custom.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".date").datepicker({
					minDate:0,
					dateFormat: "yy-mm-dd"
				});
				
				$("#btnlogin").click(function() {
					window.location.href='login.php';
				});
				
			});
			
			function show_checkout() {
				var hour_in = $("#hour_in").val();
				var hrs = $("#hours").val();
				if (hour_in !== "" && hrs !== "") {
					if (!isNaN(hour_in) && !isNaN(hrs)) {
						if (parseInt(hour_in) + parseInt(hrs) <= 22) { 
							$("#checkout").html(parseInt(hour_in) + parseInt(hrs) + ":00");
							$("#hour_out").val(parseInt(hour_in) + parseInt(hrs));
						} else {
							alert("Out Time can not more than 22:00");
						}
					} else {
						alert("Please input Number");
					}
				}
				
			}
			
			function Validate() {
				var OK = false;
				if (confirm("Are you sure?")) {
					if ($("#checkin").val()!=="" && $("#hour_in").val() !== "" && $("#hour_out").val() !== "") {
						$.ajax( {
							async: false,
							url: "static_funcs.php?opt=validateReservation",
							type: "post",
							data: $("#frm1").serialize(),
							success: function(response) {	
								if ($.trim(response)=="1") {
									OK = true;
									return OK;
								} else {
									//alert(response);
									$("#msg").html("Time is not available. Choose another");
								}
							}		
							
						});
					} else {
						alert("Plase input all necessary fields");
					}
				}
				return OK;
			}
		</script>
    </head>
    <body>
    <center>
        <div id="header">
			<table style="width:100%" border=0 cellspacing=0 cellpadding=0><tr><td>
				<h2>Welcome To Futsal Court Reservation</h2>
			</td>
				<td align="right">
				<?php
				if (!isset($_SESSION["UserID"])) {
					echo "<input type=\"button\" id=\"btnlogin\" name=\"btnlogin\" value=\"Sign In\" class=\"button\" />";
				} else {
					echo $_SESSION["name"];
				}?></td>
			</tr>
			</table>
		</div>
    </center>
        <div id="content" class="boxContent" style="display: block;">
			<form id="frm1" method="post" onsubmit="return Validate()" action="index.php">
			<input type='hidden' name='fill' value='1'>
            <table style="width:80%">
				<tr>
					<td><b>In Date</b></td>
					<td><b>Start Time</b></td><td><b>How Many Hours?</b></td><td><b>Out Time</b></td><td></td>
				</tr>
				<tr>
					<td><input type="text" class="date largefont" id="checkin" name="checkin" ></td>
					<td>
					<select id="hour_in" name="hour_in" class="text largefont" style="width:80px" onchange="show_checkout()">
					<option value=''>-</option>
					<?php
						for ($i=6;$i<=21;$i++) {
							echo "<option value='".$i."'>".$i.":00</option>";
						}
					?>
					</select></td>
					<td>
					<input type="text" class="text largefont" id="hours" name="hours"  style="width:70px" onchange="show_checkout()">
					</td>
					<td><span id='checkout' class="largefont"></span>
					<input type='hidden' id='hour_out' name='hour_out' /></td>
					<td><input type="submit" id="btnreserve" name="btnreserve" value="Reserve" class="button"/></td>
				</tr>
			</table>
			</form>
			<div id="msg"></div>
         </div>
    </body>
</html>
